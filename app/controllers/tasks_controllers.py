from flask import request, current_app, jsonify
from app.models.tasks_model import TasksModel
from app.models.tasks_categories_model import TasksCategoriesModel
from app.models.categories_model import CategoriesModel
from app.models.eisenhowers_model import EisenhowersModel
from app.models.exception import NoIDFoundError
from app.controllers.categories_controllers import create_category


def define_category(data):
    if data['importance'] == 1 and data['urgency'] == 1:
        return 1
    if data['importance'] == 1 and data['urgency'] == 2:
        return 2
    if data['importance'] == 2 and data['urgency'] == 1:
        return 3
    if data['importance'] == 2 and data['urgency'] == 2:
        return 4

def create_tasks():
    session = current_app.db.session

    data = request.get_json()
    full_table = TasksModel.query.all()
    
    name_table = []
    for item in full_table:
        name_table.append(item.name)
    
    for item in name_table:
        if item == data['name']:       
            return { 'error': 'task already exists'}, 409
        
    if data['importance'] > 2 or data['urgency'] > 2:
        return { "error": {
            "valid_options": {
                "importance": [1, 2],
                "urgency": [1, 2]
            },
            "received_options": {
                "importance": data['importance'],
                "urgency": data['urgency']
            }
            
            }}, 404

    data['eisenhower_classification'] = define_category(data)

    new_task = TasksModel(
        name = data['name'],
        description = data['description'],
        duration = data['duration'],
        importance = data['importance'],
        urgency = data['urgency'],
        eisenhower_id = data['eisenhower_classification']
    )

    eisenhower = EisenhowersModel.query.get(data['eisenhower_classification'])
    
    # CRIAÇÃO TASK_CATEGORIES
    
    search_category = data['categories']

    for item in search_category:
        category = CategoriesModel.query.filter_by(name = item['name']).first()
        if not category:
            new_category = CategoriesModel(name = item['name'], description = "")
            session.add(new_category)
            category = CategoriesModel.query.filter_by(name = item['name']).first()
        
    session.add(new_task)    
        
    for item in search_category:    
        find_category = CategoriesModel.query.filter_by(name = item['name']).first()
        find_task = TasksModel.query.filter_by(name = new_task.name).first()
        new_task_categorie = TasksCategoriesModel(
            task_id = find_task.id,
            category_id = find_category.id
        )
        session.add(new_task_categorie)
    
    session.commit()

    return jsonify({
        "name": data['name'],
        "description": data['description'],
        "duration": data['duration'],
        "importance": data['importance'],
        "urgency": data['urgency'],
        "eisenhower_classification": eisenhower.type,
        "category": search_category
    }), 201
    
def update_tasks(id: int):
    session = current_app.db.session
    data = request.get_json()

    try:
        tasks = TasksModel.query.get(id)
        
        if not tasks:
            raise NoIDFoundError
            
        for key, value in data.items():
            setattr(tasks, key, value)    
        
        parse_data = { "urgency": tasks.urgency, "importance": tasks.importance }                
        print(list(data.keys()))
        
        if parse_data['importance'] > 2 or parse_data['urgency'] > 2:
            return { "error": {
                "valid_options": {
                    "importance": [1, 2],
                    "urgency": [1, 2]
                },
                "received_options": {
                    "importance": data['importance'] if 'importance' in list(data.keys()) else "null",
                    "urgency": data['urgency'] if 'urgency' in list(data.keys()) else "null"
                }
                
                }}, 404   
                     
        tasks.eisenhower_id = define_category(parse_data)     

        eisenhower = EisenhowersModel.query.get(tasks.eisenhower_id)

        session.add(tasks)
        session.commit()

        return jsonify({
            "name": tasks.name,
            "description": tasks.description,
            "duration": tasks.duration,
            "importance": tasks.importance,
            "urgency": tasks.urgency,
            "eisenhower_classification": eisenhower.type
        }), 200
        
    except NoIDFoundError as err:
        return err.message   
    
def delete_tasks(id: int):
    
    try:
        query = TasksModel.query.get(id)

        if not query:
            raise NoIDFoundError

        current_app.db.session.delete(query)
        current_app.db.session.commit()

        return "", 204     
    
    except NoIDFoundError as err:
        return err.message