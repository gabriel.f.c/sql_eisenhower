from flask import request, current_app, jsonify
from app.models.categories_model import CategoriesModel
from app.models.exception import NoIDFoundError

def create_category():
    session = current_app.db.session

    data = request.get_json()
    full_table = CategoriesModel.query.all()
    
    name_table = []
    for item in full_table:
        name_table.append(item.name)
        
    for item in name_table:
        if item == data['name']:       
            return { 'error': 'category already exists'}, 409

    new_category = CategoriesModel(**data)

    session.add(new_category)
    session.commit()

    return jsonify({
        "id": new_category.id,
        "name": new_category.name,
        "description": new_category.description,
    }), 201
    
def update_category(id: int):
    session = current_app.db.session
    data = request.get_json()

    try:
        category = CategoriesModel.query.get(id)
        
        if not category:
            raise NoIDFoundError
            
        for key, value in data.items():
            setattr(category, key, value)    

        session.add(category)
        session.commit()

        return jsonify({
            "id": category.id,
            "name": category.name,
            "description": category.description,
        }), 200
        
    except NoIDFoundError as err:
        return err.message   
    
def delete_category(id: int):
    
    try:
        query = CategoriesModel.query.get(id)

        if not query:
            raise NoIDFoundError

        current_app.db.session.delete(query)
        current_app.db.session.commit()

        return "", 204     
    
    except NoIDFoundError as err:
        return err.message