from flask import request, current_app, jsonify
from app.models.tasks_model import TasksModel
from app.controllers.tasks_controllers import define_category
from app.models.tasks_categories_model import TasksCategoriesModel
from app.models.categories_model import CategoriesModel
from app.models.eisenhowers_model import EisenhowersModel
from app.controllers.categories_controllers import create_category


def get_all():
    category = CategoriesModel.query.all()
    full_list = []
    for item in category:
        query = TasksCategoriesModel.query.filter_by(category_id = item.id).all()
        
        task_list = [ ]
        for task in query:
            parse = TasksModel.query.filter_by(id = task.task_id).first()
            
            # Translate Eisenhower
            data = { "importance": parse.importance, "urgency": parse.urgency }
            priority = define_category(data)
            eisenhower = EisenhowersModel.query.get(priority)
            
            task_list.append({
                "id": parse.id,
                "name": parse.name,
                "description": parse.description,
                "priority": eisenhower.type,
            })
        
        category_shape = {
            "id": item.id,
            "name": item.name,
            "description": item.description,
            "tasks": task_list,
        }
        full_list.append(category_shape)
        
    return jsonify(full_list), 200    