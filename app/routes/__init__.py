from flask import Flask

def init_app(app: Flask) -> None:
  # from .estado_blueprint import bp as bp_estados
  # app.register_blueprint(bp_estados)

  from .categories_blueprint import bp as bp_categories
  app.register_blueprint(bp_categories)
  
  from .tasks_blueprint import bp as bp_tasks
  app.register_blueprint(bp_tasks)