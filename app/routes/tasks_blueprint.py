from flask import Blueprint, request, current_app, jsonify
from app.controllers.tasks_controllers import create_tasks, delete_tasks, update_tasks

bp = Blueprint("bp_tasks", __name__ )

bp.post("/tasks")(create_tasks)
bp.patch("/tasks/<int:id>")(update_tasks)
bp.delete("/tasks/<int:id>")(delete_tasks)