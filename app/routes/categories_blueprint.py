from flask import Blueprint, request, current_app, jsonify
from app.controllers.categories_controllers import create_category, delete_category, update_category, delete_category
from app.controllers.general_controllers import get_all

bp = Blueprint("bp_category", __name__ )

bp.get("/")(get_all)
bp.post("/category")(create_category)
bp.patch("/category/<int:id>")(update_category)
bp.delete("/category/<int:id>")(delete_category)