from app.configs.database import db

class TasksCategoriesModel(db.Model):
    __tablename__ = 'tasks_categories'
    
    id = db.Column(db.Integer, primary_key=True)
    task_id = db.Column(
      db.Integer,
      db.ForeignKey("tasks.id"),
      nullable=False
      )
    
    category_id = db.Column(
      db.Integer,
      db.ForeignKey("categories.id"),
      nullable=False
      )